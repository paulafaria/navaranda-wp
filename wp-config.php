<?php

if ( !defined('ABSPATH') ) define('ABSPATH', dirname(__FILE__) . '/');
if ( !defined('WP_ENV') ) define('WP_ENV', (($_ENV['ENV'] ?: getenv('ENV')) ?: 'production'));

require_once(ABSPATH . '/environments/' . WP_ENV . '.php');
require_once(ABSPATH . 'wp-settings.php');
