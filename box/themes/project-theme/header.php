<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <?php theme_meta(); ?>
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php wp_head(); ?>
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
