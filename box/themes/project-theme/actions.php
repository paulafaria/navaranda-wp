<?php

// XSS HTTP/HTTPS Sanitization

add_action('init', 'xssHandling');

function xssHandling() {
  foreach(array($_GET, $_POST, $_REQUEST) as $httpConst) XSSSanitization( $httpConst );
}

function XSSSanitization(&$param) {
  if(!is_array( $param ) && is_string( $param ) ) $param = filter_var($param, FILTER_SANITIZE_STRING);
  else if( is_array( $param ) ) foreach( $param as $key => $value ) XSSSanitization( $param[$key] );
}

// MANAGE MENU ----------------------------------
add_action('admin_menu', 'remove_menus');
function remove_menus() {
  remove_menu_page('index.php'); // Dashboard
  remove_menu_page('edit.php'); // Posts
  remove_menu_page('edit-comments.php'); // Comments
}

// SET CUSTOM LOGO TO LOGIN FORM ----------------------------------
add_action( 'login_head', 'custom_login_logo' );
function custom_login_logo() {
  $logo = 'admin_logo.png';
  if(!is_null($logo)) {
    echo '<style type="text/css">
      h1 a {
        background-size: 180px 52px !important;
        width: 180px !important;
        height: 52px !important;
        background-image:url('.get_bloginfo('template_directory').'/images/'.$logo.') !important; }

      .login .message {
        border-left-color: #000 !important;
      }

      .button-primary {
        background-color: #000 !important;
        border-color: #000 !important;
        box-shadow: none !important;
        color: white !important;
        text-shadow: none !important;
      }
      </style>';
    }
}

// FIX TOP MENU GAP ----------------------------------
add_action( 'admin_head', 'clean_ui' );
function clean_ui() {
  echo '
    <style type="text/css">
      #adminmenu { margin-top: 0 !important; }
      #adminmenu .wp-first-item { display: none; }
      .wp-menu-separator { display: none !important; }
    </style>
  ';
}

// REMOVE DASHBOARD WIDGETS ----------------------------------
remove_action('welcome_panel', 'wp_welcome_panel'); //remove WordPress Welcome Panel
add_action('admin_init', 'remove_dashboard_widgets');
function remove_dashboard_widgets() {
  remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); // At a Glance
  remove_meta_box('dashboard_quick_press', 'dashboard', 'normal'); // Quick Draft
  remove_meta_box('dashboard_primary', 'dashboard', 'core'); // WordPress News
  remove_meta_box('dashboard_secondary', 'dashboard', 'core'); // WordPress News
  remove_meta_box('dashboard_activity', 'dashboard', 'normal'); // Activity
  remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
  remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core'); // Drafts Widget
  remove_meta_box('dashboard_incoming_links', 'dashboard', 'core'); // Incoming Links Widget
  remove_meta_box('dashboard_plugins', 'dashboard', 'core'); // Plugins Widget
}

// DASHBOARD REDIRECT ----------------------------------
add_action( 'wp_dashboard_setup', 'dashboard_redirect' );
function dashboard_redirect() {
  echo "<script>window.location = '/wp-admin/admin.php?page=home-settings';</script>";
}

// REMOVE UPDATE NAG ----------------------------------
add_action('admin_menu','wphidenag');
function wphidenag() {
  remove_action( 'admin_notices', 'update_nag', 3);
}

// REMOVE WORDPRESS JUNKS ----------------------------------
remove_action( 'wp_head', 'wp_generator' );     //  wordpress version from header
remove_action( 'wp_head', 'rsd_link' );       // link to Really Simple Discovery service endpoint
remove_action( 'wp_head', 'wlwmanifest_link' );   // link to Windows Live Writer manifest file
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'start_post_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action( 'template_redirect', 'wp_shortlink_header', 11 ); // Remove WordPress Shortlinks from HTTP Headers

// MOVE JAVASCRIPTS FROM HEADER TO FOOTER TO SPEED PAGE LOADING ----------------------------------
// remove_action('wp_head', 'wp_print_scripts');
// remove_action('wp_head', 'wp_print_head_scripts', 9);
// remove_action('wp_head', 'wp_enqueue_scripts', 1);
// add_action('wp_footer', 'wp_print_scripts', 5);
// add_action('wp_footer', 'wp_enqueue_scripts', 2);
// add_action('wp_footer', 'wp_print_head_scripts', 5);


// ENQUEUE STYLES ON HEAD FOR FRONT END. ----------------------------------
add_action( 'wp_enqueue_scripts', 'enqueue_styles_cb', 11 );
if (!function_exists('enqueue_styles_cb')){
  function enqueue_styles_cb() {
    // then load our main stylesheet
    wp_enqueue_style( 'theme-style', CSS_URL . "main.css" );
    wp_enqueue_style( 'theme-style', CSS_URL . "critical.css" );
  }
}

// ENQUEUE SCRIPTS AT THE END FOR FRONT END. ----------------------------------
add_action( 'wp_enqueue_scripts', 'enqueue_scripts_cb' );
if (!function_exists('enqueue_scripts_cb')){
  function enqueue_scripts_cb() {
    if (!is_admin()) {
      // add JavaScript to pages with the comment form to enable threaded comments
      if (is_singular() AND comments_open() AND (get_option('thread_comments') === 1)) {
        wp_enqueue_script('comment-reply');
      }

      wp_register_script( 'app', JS_URL . 'app.min.js', false, '1.0', true );
      wp_enqueue_script( array('app'));
    }
  }
}

function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('view-site');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('new-content');
}

add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );

function admin_color_scheme() {
   global $_wp_admin_css_colors;
   $_wp_admin_css_colors = 0;
}

add_action('admin_head', 'admin_color_scheme');

/**
 * NONCE / AJAX
 */

add_action( 'wp_enqueue_scripts', 'secure_enqueue_script' );

function secure_enqueue_script() {
  wp_register_script( 'secure-ajax-access', esc_url( add_query_arg( array( 'environment' => 1 ), site_url() ) ) );
  wp_enqueue_script( 'secure-ajax-access' );
}

add_action( 'template_redirect', 'securely_inject_localized_vars' );

function securely_inject_localized_vars() {
  if ( !isset( $_GET[ 'environment' ] ) ) return;

  $localized_vars = array(
    'xhr_url'             => admin_url('admin-ajax.php'),
    'FACEBOOK_APP_ID'     => FACEBOOK_APP_ID,
    'FACEBOOK_TAB_URL'    => FACEBOOK_TAB_URL,
    'FACEBOOK_CANVAS_URL' => FACEBOOK_CANVAS_URL,
    'GA'                  => GA,
    'IMG_URL'             => IMG_URL,
    'WP_SITEURL'          => WP_SITEURL,
    'nonce_name'          => wp_create_nonce('nonce_name') // Create nonce key and add expose it to javascript, duplicate this line for each nonce key needed.
  );

  $localized_var_array = array();
  foreach( $localized_vars as $var => $value ) $localized_var_array[] = esc_js( $var ) . " : '" . esc_js( $value ) . "'";

  header("Content-type: application/x-javascript");
  printf( 'var %s = {%s};', 'enviroment', implode( ',', $localized_var_array ) );
  exit;
}

add_ajax_nonce_action('action_name'); // Duplicate this call for each custom action that have nonce validation

/**
 * Add two actions for each custom action request using nonce validation
 * @param String $action The name of custon action
 * @return Void
 */
function add_ajax_nonce_action($action) {
  add_action('wp_ajax_nopriv_'.$action, $action);
  add_action('wp_ajax_'.$action, $action);
}

/**
 * Custom action for handle xhr request using nonce validation
 * @return Void
 */
function action_name() {
  check_ajax_referer( 'nonce_name', 'nonce' ); // First argument is nonce variable name coming from xhr request and the second always is nonce.
  die();
}

// ADD META TAGS ON THE HEADER ----------------------------------
add_action( 'theme_meta', 'meta_cb' );
if (!function_exists('meta_cb')){
  function meta_cb(){
    echo "<meta charset='".get_bloginfo('charset')."'>\n";
    echo "<meta name='description' content='".get_bloginfo('description')."'>\n";
    echo "<meta name='author' content='frowea'>\n";
    echo "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>\n";
    echo "<meta name='viewport' content='initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />\n";
    //echo "<meta name='viewport' content='width=1024' />\n"; // for testing purpose
  }
}
//load meta in header section by calling theme_meta()
function theme_meta(){
  do_action('theme_meta');
}

// function wpb_custom_logo() {
// echo '
// <style type="text/css">
//   #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
//   background-image: url(' . get_bloginfo('stylesheet_directory') . '/images/admin_logo_white.png) !important;
//   background-position: 0 0;
//   color:rgba(0, 0, 0, 0);
//   }
//   #wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
//   background-position: 0 0;
//   }
// </style>
// ';
// }
//
// //hook into the administrative header output
// add_action('wp_before_admin_bar_render', 'wpb_custom_logo');
