var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    sassGlob    = require('gulp-sass-glob'),
    sourcemaps  = require('gulp-sourcemaps'),
    imagemin    = require('gulp-imagemin'),
    changed     = require('gulp-changed'),
    pngquant    = require('imagemin-pngquant'),
    uglify      = require('gulp-uglify')
    do_concat   = require('gulp-concat'),
    gutil       = require('gulp-util'),
    browserSync = require('browser-sync').create();


var config = { "site_url": "http://wpstarter.dev" };

/** Compila SASS -> CSS e minifica  **/
gulp.task('sass', function () {
  gulp.src(['sass/**/*.scss'])
    .pipe(sassGlob())
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('assets/css'));
});

/** Minifica imagens e joga dentro da pasta prod/imgages  **/
gulp.task('images', function() {
    gulp.src('images/**/*')
        .pipe(changed('assets/images'))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('assets/images'))
});

/** Compress JS **/
gulp.task('compress', function() {
  gulp.src(['js/vendor/*.js', 'js/*.js'])
      .pipe(do_concat('app.min.js'))
      .pipe(uglify().on('error', gutil.log))
      .pipe(gulp.dest('assets/js'));
});

/** Inicia BrowserSync no watch e escuta arquivos js e css  **/
gulp.task('browser-sync', function() {
    browserSync.init(["assets/css/*.css", "assets/js/*.js"], {
        proxy: config.site_url
    });
});

gulp.task('default', ['browser-sync'], function () {
    gulp.watch('sass/**/*.scss', ['sass']);
    gulp.watch('images/**/*', ['images']);
    gulp.watch('js/**/*.js', ['compress']);
});
