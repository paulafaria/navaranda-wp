<?php

// Escapando todos os helpers do WordPress
add_filter('the_content', 'wp_kses_post');
add_filter('the_title', 'esc_html');
add_filter('the_excerpt', 'esc_html');
add_filter('the_permalink', 'esc_url');

add_filter('get_the_content', 'wp_kses_post');
add_filter('get_the_title', 'esc_html');
add_filter('get_the_excerpt', 'esc_html');
add_filter('get_the_permalink', 'esc_url');

function clear_nbsp($content){
  return str_replace("<p>&nbsp;</p>","", $content);
}

add_filter('the_content', 'clear_nbsp');


function remove_footer_admin () {
  echo 'Copyright ® 2017';
}

add_filter('admin_footer_text', 'remove_footer_admin');

add_filter( 'update_footer', 'change_footer_version', 9999 );

function change_footer_version() {
  return '1.0';
}

// remove Admin bar
add_filter('show_admin_bar', 'show_admin_bar_cb');
if ( ! function_exists( 'show_admin_bar_cb' ) ) {
  function show_admin_bar_cb(){
    return false;
  }
}

// REMOVE VERSION NUMBERS FROM CSS AND JS SRC ----------------------------------
add_filter( 'style_loader_src', 'wp_remove_versions', 9999 );
add_filter( 'script_loader_src', 'wp_remove_versions', 9999 );
function wp_remove_versions( $src ) {
  if ( strpos( $src, 'ver=' ) ) {
    $src = remove_query_arg( 'ver', $src );
  }
  return $src;
}

// REMOVE VERSION NUMBERS FROM ALL HTML, COMMENTS AND TAGS IN HEAD ----------------------------------
add_filter( 'get_the_generator_html', '__return_false' );
add_filter( 'get_the_generator_xhtml', '__return_false' );
add_filter( 'get_the_generator_atom', '__return_false' );
add_filter( 'get_the_generator_rss2', '__return_false' );
add_filter( 'get_the_generator_rdf', '__return_false' );
add_filter( 'get_the_generator_comment', '__return_false' );
add_filter( 'get_the_generator_export', '__return_false' );


// FILTER wp_title() FOR BETTER SEO ----------------------------------
add_filter( 'wp_title', 'theme_wp_title', 10, 2 );
if(!function_exists('theme_wp_title')){
  function theme_wp_title( $title, $sep) {

    global $paged, $page;

    if ( is_feed() ) return $title;
    // Add the site name.
    $name = get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );

    // find the type of index page this is
    if ( $site_description && ( is_home() || is_front_page() ) ) $title = "$title $name $sep $site_description";
    elseif( is_category() ) $title = "$title Category $sep $name";
    elseif( is_tag() ) $title = "$title Tag $sep $name";
    elseif( is_author() ) $title = "$title  Author $sep $name";
    elseif( is_year() || is_month() || is_day() ) $title = "$title Archives $sep $name";
    else $title = "$title $name $sep $site_description";

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 )
      $title = "$title $name $sep " . sprintf( __( 'Page %s', TX_DOMAIN ), max( $paged, $page ) );

    return $title;

  }
}
