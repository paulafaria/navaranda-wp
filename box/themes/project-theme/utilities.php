<?php

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Home',
		'menu_title'	=> 'Home',
		'menu_slug' 	=> 'home-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position'    => '7',
		'icon_url'		=> 'dashicons-tagcloud'
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Sobre',
		'menu_title'	=> 'Sobre',
		'menu_slug' 	=> 'sobre-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position'    => '14',
		'icon_url'		=> 'dashicons-nametag'
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Contato',
		'menu_title'	=> 'Contato',
		'menu_slug' 	=> 'contato-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position'    => '15',
		'icon_url'		=> 'dashicons-megaphone'
	));
}
