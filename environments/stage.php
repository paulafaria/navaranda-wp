<?php
/**
 * STAGING ENVIRONMENT WORDPRESS CONFIG FILE
 * @author Vitorino <vitorino@tomcomunicacao.com.br>
 */

 // Copy this set of variables and configure your apache server with the desired ones

// SetEnv ENV 
// SetEnv DB_NAME
// SetEnv DB_USER
// SetEnv DB_PASS
// SetEnv DB_HOST
// SetEnv SSL_ENABLE
// SetEnv DOMAIN
// SetEnv WP_CONTENT_DIR_NAME
// SetEnv AWS_ACCESS_KEY_ID
// SetEnv AWS_SECRET_ACCESS_KEY
// SetEnv EXTERNAL_DB_USER
// SetEnv EXTERNAL_DB_PASSWORD
// SetEnv EXTERNAL_DB_NAME
// SetEnv EXTERNAL_DB_HOST

define( 'DB_NAME',     $_ENV['DB_NAME'] ?: getenv( 'DB_NAME' ) );
define( 'DB_USER',     $_ENV['DB_USER'] ?: getenv( 'DB_USER' ) );
define( 'DB_PASSWORD', $_ENV['DB_PASS'] ?: getenv( 'DB_PASS' ) );
define( 'DB_HOST',     $_ENV['DB_HOST'] ?: getenv( 'DB_HOST' ) );
define( 'TABLE_PREFIX',     $_ENV['TABLE_PREFIX'] ?: getenv( 'TABLE_PREFIX' ) );
define( 'SSL_ENABLE',  $_ENV['SSL_ENABLE'] ?: getenv( 'SSL_ENABLE' ) );
define( 'DB_CHARSET',  'utf8' );
define( 'DB_COLLATE',  'utf8_general_ci' );
define( 'DOMAIN_URL', (SSL_ENABLE === "true" ? 'https://' : 'http://') . ( $_ENV['DOMAIN'] ?: getenv( 'DOMAIN' ) ) );
define( 'WP_CONTENT_DIRECTORY', ( $_ENV['WP_CONTENT_DIR_NAME'] ?: getenv( 'WP_CONTENT_DIR_NAME' ) ) );

// LEGACY DATABASE
define( 'EXTERNAL_DB_USER',     $_ENV['EXTERNAL_DB_USER']     ?: getenv( 'EXTERNAL_DB_USER' ) );
define( 'EXTERNAL_DB_PASSWORD', $_ENV['EXTERNAL_DB_PASSWORD'] ?: getenv( 'EXTERNAL_DB_PASSWORD' ) );
define( 'EXTERNAL_DB_NAME',     $_ENV['EXTERNAL_DB_NAME']     ?: getenv( 'EXTERNAL_DB_NAME' ) );
define( 'EXTERNAL_DB_HOST',     $_ENV['EXTERNAL_DB_HOST']     ?: getenv( 'EXTERNAL_DB_HOST' ) );

// AWS 

define( 'AWS_ACCESS_KEY_ID', $_ENV['AWS_SECRET_ACCESS_KEY']     ?: getenv( 'AWS_SECRET_ACCESS_KEY' ) );
define( 'AWS_SECRET_ACCESS_KEY', $_ENV['AWS_SECRET_ACCESS_KEY'] ?: getenv( 'AWS_SECRET_ACCESS_KEY' ) );

// SPECIFIC ENVIRONMENT CONFIGURATION

define( 'WP_HOME',                 DOMAIN_URL);
define( 'WP_SITEURL',              WP_HOME);
define( 'FACEBOOK_APP_ID',         '' ); // Facebook App ID
define( 'FACEBOOK_TAB_URL',        '' ); // Facebook Tab URL
define( 'FACEBOOK_CANVAS_URL',     '' ); // Facebook Canvas URL
define( 'GA',                      '' ); // Google Analytics
define( 'WP_MEMORY_LIMIT',         '2048M' );
define( 'WP_MAX_MEMORY_LIMIT',     '2048M' );
define( 'WPLANG',                  'pt_BR' );
define( 'WP_CACHE',                false);
define( 'DISABLE_WP_CRON',         false);
define( 'DISALLOW_FILE_MODS',      false);
define( 'WP_HTTP_BLOCK_EXTERNAL',  false );
define( 'WP_ACCESSIBLE_HOSTS',     '' ); // Add the hosts allowed to make requests ex: facebook.com
define( 'WP_POST_REVISIONS',       false);
define( 'WP_CONTENT_URL', WP_HOME . '/' . WP_CONTENT_DIRECTORY);
define( 'WP_CONTENT_DIR', ABSPATH . '/' . WP_CONTENT_DIRECTORY);
define( 'FORCE_SSL_LOGIN', (SSL_ENABLE === "true") );
define( 'FORCE_SSL_ADMIN', (SSL_ENABLE === "true") );
define( 'DISALLOW_FILE_EDIT', true );
define( 'FS_METHOD', 'direct' );

// Generate custom keys for each environment at https://api.wordpress.org/secret-key/1.1/salt/

define('AUTH_KEY',         '');
define('SECURE_AUTH_KEY',  '');
define('LOGGED_IN_KEY',    '');
define('NONCE_KEY',        '');
define('AUTH_SALT',        '');
define('SECURE_AUTH_SALT', '');
define('LOGGED_IN_SALT',   '');
define('NONCE_SALT',       '');

$table_prefix  = TABLE_PREFIX; // CHOOSE A RANDOM STRONG PREFIX TO IMPROVE TABLE SECURITY

// Reverse Proxy
if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ) {
  $_SERVER['HTTPS'] = 'on';
}
